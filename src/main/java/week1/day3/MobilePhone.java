package week1.day3;

public interface MobilePhone {
	public void sendSms(String txt);
	public void sendEmail();
}
