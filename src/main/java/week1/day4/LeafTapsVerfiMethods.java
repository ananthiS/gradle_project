package week1.day4;

import java.util.List;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LeafTapsVerfiMethods {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		//enter login name
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		//To get the x,y axis of the username
		Point uNameloc = driver.findElementById("username").getLocation();
		System.out.println(uNameloc);
		int xaxis = driver.findElementById("username").getLocation().getX();

		System.out.println("X axis of the username is " +xaxis);
		
		int yaxis = driver.findElementById("username").getLocation().getY();
		System.out.println("Y axis of the username is " +yaxis);
		
		
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		//To display the currenturl
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);
		
		//To get the title of the page
		String title = driver.getTitle();
		System.out.println(title);
		
		String crmText = driver.findElementByLinkText("CRM/SFA").getText();
		System.out.println("The first link is "+ crmText);
		
		
		//To click CRMSFA
		
		driver.findElementByLinkText("CRM/SFA").click();
		//To click Leads tab
		driver.findElementByLinkText("Leads").click();
		//To click Create Lead
		driver.findElementByLinkText("Create Lead").click();
		
		driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");
		driver.findElementById("createLeadForm_firstName").sendKeys("Ananthi");
		driver.findElementById("createLeadForm_lastName").sendKeys("Shanmugam");
		//selecting values from the dropdown using Select class
		
		WebElement sourceElement = driver.findElementById("createLeadForm_dataSourceId");
		Select sc = new Select(sourceElement);
		
		List<WebElement> sourceDropdown = sc.getOptions();
		sc.selectByVisibleText("Partner");
		//To check the value from the dropdown
		String srcattribute = sourceElement.getAttribute("value");
		
		
		System.out.println(sourceElement.getAttribute("value"));
		
		
		

	}

}
