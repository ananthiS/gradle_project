package week1.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class LeaftapsLogin {

	//public static void main(String[] args) {
	@Test(invocationCount=2)
	public void createLead() {
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver= new ChromeDriver();
	//To load URL
	driver.get("http://leaftaps.com/opentaps/control/main");
	driver.manage().window().maximize();
	//enter login name
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	
	//To click CRMSFA
	
	driver.findElementByLinkText("CRM/SFA").click();
	//To click Leads tab
	driver.findElementByLinkText("Leads").click();
	//To click Create Lead
	driver.findElementByLinkText("Create Lead").click();
	
	driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");
	driver.findElementById("createLeadForm_firstName").sendKeys("Ananthi");
	driver.findElementById("createLeadForm_lastName").sendKeys("Shanmugam");
	//selecting values from the dropdown using Select class
	
	WebElement sourceElement = driver.findElementById("createLeadForm_dataSourceId");
	Select sc = new Select(sourceElement);
	
	List<WebElement> sourceDropdown = sc.getOptions();
	sc.selectByVisibleText("Partner");
	

	
	int sourceCount= sourceDropdown.size();
	
	for (WebElement eachoption:sourceDropdown) {
		System.out.println(eachoption.getText());
	}
	
	//select using selectbyvalue
	WebElement mrktCampId = driver.findElementById("createLeadForm_marketingCampaignId");
	
	
	Select sc1=new Select(mrktCampId);
	List<WebElement> mrktCampDropdown = sc1.getOptions();
	sc1.selectByValue("DEMO_MKTG_CAMP");
	
	//driver.findElementById("createLeadForm_dataSourceId").sendKeys("Existing Customer");
	
	//driver.findElementById("createLeadForm_marketingCampaignId").sendKeys("Demo Marketing Campaign");
	driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Anu");
	driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Vijay");
	driver.findElementById("createLeadForm_personalTitle").sendKeys("Salutation");
	driver.findElementById("createLeadForm_generalProfTitle").sendKeys("First Lead");
	driver.findElementById("createLeadForm_departmentName").sendKeys("Computer");
	driver.findElementById("createLeadForm_annualRevenue").sendKeys("3434");
	driver.findElementById("createLeadForm_currencyUomId").sendKeys("INR - Indian Rupee");
	
	
	//select the last value from the industry dropdown
	WebElement indust = driver.findElementById("createLeadForm_industryEnumId");
	Select sc2=new Select(indust);
	List<WebElement> industDropdown = sc2.getOptions();
    int industCnt=	industDropdown.size();
    System.out.println(industCnt);
    sc2.selectByIndex(industCnt-1);
	
	
	
	//driver.findElementById("createLeadForm_industryEnumId").sendKeys("Computer Software");
	
	driver.findElementById("createLeadForm_ownershipEnumId").sendKeys("Sole Proprietorship");
	driver.findElementById("createLeadForm_numberEmployees").sendKeys("54");
	driver.findElementById("createLeadForm_sicCode").sendKeys("SIC Code");
	driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Ticker Symbol");
	driver.findElementById("createLeadForm_description").sendKeys("sdsdsdsddsdsdsdsd");
	driver.findElementById("createLeadForm_importantNote").sendKeys("sdsadsadsadsadsdd");
	driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("2");
	driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("073");
	
	driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9566758373");
	driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("233");
	
	driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Bala");
	
	driver.findElementById("createLeadForm_primaryEmail").sendKeys("shan.ananthi2002@gmail.com");
	
	driver.findElementById("createLeadForm_generalToName").sendKeys("Bhama");
	driver.findElementById("createLeadForm_generalAttnName").sendKeys("Srinika");
	driver.findElementById("createLeadForm_generalAddress1").sendKeys("sadsdsdsd");
	driver.findElementById("createLeadForm_generalAddress2").sendKeys("sdsadsad3rree");
	driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
	driver.findElementById("createLeadForm_generalStateProvinceGeoId").sendKeys("Indiana");
	
	driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600073");
	driver.findElementById("createLeadForm_generalCountryGeoId").sendKeys("India");
	
	driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("544");
	
	
	
	
	
	
	driver.findElementByName("submitButton").click();
	
	String fname =driver.findElementById("viewLead_firstName_sp").getText();
	if (fname.equals("Ananthi")){
		System.out.println("Create Lead successfully created for " + fname);
	}
	else
	{
		System.out.println("Lead Cannot be created");
	}
	
	
	
	
	
	
	
	
		

	}

}
