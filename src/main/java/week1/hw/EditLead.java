package week1.hw;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class EditLead {

	//public static void main(String[] args) throws InterruptedException {
	@Test(dependsOnMethods="week1.day2.LeaftapsLogin.createLead")	
	public void editLead() throws InterruptedException {
		String fName;
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		//enter login name
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByLinkText("Find Leads").click();
		
		Thread.sleep(3000);
		
		fName="Ananthi";
		
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(fName);
		
		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
	
		Thread.sleep(3000);
		//To click the first resulting lead
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		
		
		
		//Thread.sleep(3000);
		String title = driver.getTitle();
		if (title.equals("View Lead | opentaps CRM") ){
			System.out.println(title + " has been verified");
		}
		
		driver.findElementByXPath("//a[contains(text(),'Edit')]").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("TestLeaf New");		
		
		driver.findElementByXPath("//input[@value='Update']").click();
		
		String text = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println(text);
		if (text.contains("TestLeaf New")) {
		
			System.out.println("Company name updated successfully");
		}
		
	
	}

}
