package week1.hw;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DuplicateLead {

	//public static void main(String[] args) throws InterruptedException {
	@Test(enabled=false)
	public void duplicateLead() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		
		//enter login name
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByLinkText("Find Leads").click();
		
		driver.findElementByXPath("//span[contains(text(),'Email')]").click();
		
		driver.findElementByXPath("//input[@name='emailAddress']").sendKeys("anu123@gmail.com");
		
		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
		
		Thread.sleep(3000);
					
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		
		String fName = driver.findElementByXPath("//span[@id='viewLead_firstName_sp']").getText();
		System.out.println(fName);
		
		
		driver.findElementByXPath("//a[contains(text(),'Duplicate Lead')]").click();
		
		String title = driver.getTitle();
		if (title.equals("Duplicate Lead | opentaps CRM") ){
			System.out.println(title + " has been verified");
		}
		
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		
		String newfName = driver.findElementByXPath("//span[@id='viewLead_firstName_sp']").getText();
		System.out.println(newfName);
		if (fName.equals(newfName)) {
			System.out.println("Lead duplicated successfully");
		}
		
		
		driver.close();


	}

}
