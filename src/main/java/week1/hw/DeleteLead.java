package week1.hw;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DeleteLead {

	//public static void main(String[] args) throws InterruptedException {
	@Test(dependsOnMethods="week1.day2.LeaftapsLogin.createLead")
	
	public void deleteLead() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		
		//enter login name
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByLinkText("Find Leads").click();
		
			
		driver.findElementByXPath("//span[contains(text(),'Phone')]").click();
		driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys("9566758373");
		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
		
		Thread.sleep(3000);
		String leadID = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
		System.out.println(leadID);
		
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		
		driver.findElementByXPath("//a[contains(text(),'Delete')]").click();
		
		
		driver.findElementByLinkText("Find Leads").click();
		
		
		
		driver.findElementByXPath("//input[@name='id']").sendKeys(leadID);
		
		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
		Thread.sleep(3000);
		String noRecMsg = driver.findElementByXPath("//div[contains(text(),'No records to display')]").getText();
		System.out.println(noRecMsg);
		
	}

}
