package week1.day1;

public class Television {
	int chnelNum;
	String manufacturer;
	
	public Television(String manufacturer) {
		this.manufacturer = manufacturer;
		
	}

	public void switchOn() {
		System.out.println(this.manufacturer +" Television is Switched On");
	}

	public void switchOff() {
		System.out.println(this.manufacturer +"Television is Switched Off");

	}

	public void changeChannel(int chnlNum) {
		chnelNum = chnlNum;
		System.out.println("Channel changed " + chnlNum);
	}

	public int getChannel() {
		return chnelNum;

	}
	
	public static void Destroy(Television tv) {
		System.out.println(tv.manufacturer + " is destroyed");
		
	}

}
