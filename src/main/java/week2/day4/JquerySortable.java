package week2.day4;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;



public class JquerySortable {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("https://jqueryui.com");
		driver.manage().window().maximize();
		driver.findElementByLinkText("Sortable").click();
		driver.switchTo().frame(driver.findElementByClassName("demo-frame"));
		
		WebElement item2 = driver.findElementByXPath("//li[text()='Item 2']");
		WebElement item5 = driver.findElementByXPath("//li[text()='Item 5']");
		int xloc=item5.getLocation().getX();
		int yloc=item5.getLocation().getY();
		
		Actions builder=new Actions(driver);
		//builder.dragAndDrop(item2, item5);
		
		//builder.click(item2).perform();
		
		builder.clickAndHold(item2).perform();
		
		
		
		
		
		

	}

}
