package week2.day4;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ActionsIndeedPage {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("https://www.indeed.co.in/Fresher-jobs");
		driver.manage().window().maximize();
		
		Actions builder = null;
	
		
		
		
		//WebElement firstLink = driver.findElementByXPath("(//a[@data-tn-element='jobTitle'])[1]");
		//builder.sendKeys(Keys.CONTROL);
		//builder.click(firstLink).perform();
		
		List<WebElement> allLinks = driver.findElementsByXPath("//a[@data-tn-element='jobTitle']");
		
		for (WebElement eachlink : allLinks) {
			
			WebDriverWait wait= new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.elementToBeClickable(eachlink));		
			
			eachlink.sendKeys(Keys.CONTROL,Keys.ENTER);
			Set<String> allWindows=driver.getWindowHandles();
			System.out.println(allWindows.size());
			List<String> list=new ArrayList<String>();
			list.addAll(allWindows);
			driver.switchTo().window(list.get(1));
			System.out.println(driver.getTitle());
			driver.close();
			driver.switchTo().window(list.get(0));
		}
		
		
		
		

	}
	
	
}
