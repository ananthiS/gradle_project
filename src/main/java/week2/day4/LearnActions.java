package week2.day4;

import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnActions {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("https://jqueryui.com");
		driver.manage().window().maximize();
		driver.findElementByLinkText("Draggable").click();
		driver.switchTo().frame(driver.findElementByClassName("demo-frame"));
		
		WebElement eleDrag = driver.findElementById("draggable");
		
		Point locDrag= eleDrag.getLocation();
		System.out.println(locDrag);
		
		Actions builder=new Actions(driver);
			
		builder.dragAndDropBy(eleDrag,92,98).perform();
		
		driver.switchTo().defaultContent();
		driver.findElementByLinkText("Selectable").click();
		
		driver.switchTo().frame(driver.findElementByClassName("demo-frame"));
		
		WebElement item1 = driver.findElementByXPath("//li[text()='Item 1']");
		WebElement item4 = driver.findElementByXPath("//li[text()='Item 4']");
		WebElement item7 = driver.findElementByXPath("//li[text()='Item 7']");
		
		//builder.sendKeys(Keys.CONTROL);
		
		builder.keyDown(Keys.CONTROL);
		builder.click(item1).perform();
		
		builder.click(item4).perform();
	
		builder.click(item7).perform();
		
		
		
		
		
		
		
		
		
		

	}

}
