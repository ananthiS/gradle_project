package week2.hw;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class MyntraSunglass {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("https://www.myntra.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementByClassName("desktop-searchBar").sendKeys("Sunglasses");
		driver.findElementByClassName("desktop-submit").click();
		driver.findElementByXPath("//label[contains(text(),'40%')]").click();

	}

}
