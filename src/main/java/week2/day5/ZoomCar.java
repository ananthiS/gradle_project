package week2.day5;




import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class ZoomCar {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("(//div[@class='items'])[1]").click();
		driver.findElementByClassName("proceed").click();
		

		//get the current date
		Date date=new Date();
		DateFormat sdf=new SimpleDateFormat("dd");
		String today=sdf.format(date);
		int tomoDate=Integer.parseInt(today)+1;
		System.out.println(tomoDate);
		
		//using dynamic xpath ----to pass the value from a variable
		driver.findElementByXPath("//div[contains(text(),'" +tomoDate+"')]").click();
		
		driver.findElementByClassName("proceed").click();
		driver.findElementByClassName("proceed").click();
		
		
		
		List<WebElement> noOfCars = driver.findElementsByClassName("price");
		System.out.println(noOfCars.size());
		
		//System.out.println(driver.findElementByClassName("price").getText());
		
		List<Integer> amtList = new ArrayList<Integer>();
		
		for (WebElement eachCar : noOfCars) {
			String eachStr = eachCar.getText();
			String eachPrice = eachStr.replaceAll("\\D", "");
			int amt=Integer.parseInt(eachPrice);
			//System.out.println(amt);
			amtList.add(amt);		
			
		}
		
		Integer max = Collections.max(amtList);
		System.out.println(max);
		String carModel = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/preceding::h3[1]").getText();
		System.out.println(carModel);
		
		driver.findElementByXPath("//div[contains(text(),'"+max+"')]/following::button[1]").click();
		
		driver.close();
		

		
		
		
}

}
