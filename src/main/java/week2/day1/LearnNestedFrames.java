package week2.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnNestedFrames {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_frame_cols");
		driver.manage().window().maximize();
		List<WebElement> frames = driver.findElementsByTagName("iframe");
		int size = frames.size();
		System.out.println("No. of Frames "+ size);
		
		driver.close();
		// TODO Auto-generated method stub

	}

}
