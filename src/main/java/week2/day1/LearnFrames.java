package week2.day1;

import java.util.List;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrames {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("https://jqueryui.com");
		driver.manage().window().maximize();
		
		driver.findElementByLinkText("Draggable").click();
		
		WebElement frameName = driver.findElementByClassName("demo-frame");
		
		driver.switchTo().frame(frameName);
		Point locDrag= driver.findElementById("draggable").getLocation();
		System.out.println(locDrag);
		
		String text = driver.findElementById("draggable").getText();
		System.out.println(text);
		
		driver.switchTo().defaultContent();
		
		//To get the count of the frames
		
		List<WebElement> frames = driver.findElementsByTagName("iframe");
		int size = frames.size();
		System.out.println("No. of Frames "+ size);
	
		// TODO Auto-generated method stub

	}

}
