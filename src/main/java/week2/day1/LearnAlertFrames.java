package week2.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlertFrames {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		
		driver.switchTo().frame("iframeResult");
		
		driver.findElementByXPath("//button[text()='Try it']").click();
		driver.switchTo().alert();
		driver.switchTo().alert().sendKeys("Test Leaf");
		driver.switchTo().alert().accept();
		
		//Thread.sleep(2000);
		//driver.switchTo().frame("iframeResult");
		
		String alertmsg= driver.findElementById("demo").getText();
		System.out.println(alertmsg);
		
		if (alertmsg.contains("Test Leaf")) {
			System.out.println("Alert msg verified");
		}
	
		driver.close();
		// TODO Auto-generated method stub

	}

}
