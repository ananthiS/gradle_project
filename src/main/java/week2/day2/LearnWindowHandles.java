package week2.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowHandles {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		
		driver.findElementByLinkText("Contact Us").click();
		
		System.out.println("Title of the first window : " +driver.getTitle());
		
		//to get all the windows handles
		
		Set<String> allWindows=driver.getWindowHandles();
		
		List<String> list=new ArrayList<String>();
		list.addAll(allWindows);
		
		driver.switchTo().window(list.get(1));
		System.out.println("Title of the second window: " +driver.getTitle());
		
		//close the parent/first window		
		
		driver.switchTo().window(list.get(0)).close();
	}

}
