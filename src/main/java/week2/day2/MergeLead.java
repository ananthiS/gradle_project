package week2.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class MergeLead {

	//public static void main(String[] args) throws InterruptedException {
	@Test
	public void mergeLead() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");

		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		//enter login name
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		//To click CRMSFA
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		
		
		//get window handles and store it in a list
		
		Set<String> allWindows=driver.getWindowHandles();
		List<String> winList= new ArrayList<String>();
		winList.addAll(allWindows);
		
		driver.switchTo().window(winList.get(1));
		
		driver.findElementByName("firstName").sendKeys("A");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(3000);
		
		
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		
		driver.switchTo().window(winList.get(0));
		
		String LeadID=driver.findElementByXPath("//input[@name='ComboBox_partyIdFrom']").getText();
		System.out.println(LeadID);
		
		System.out.println(driver.getTitle());
		
		driver.findElementByLinkText("Merge").click();
		driver.switchTo().alert();
		driver.switchTo().alert().accept();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		
		Set<String> allWindows1=driver.getWindowHandles();
		List<String> winList1= new ArrayList<String>();
		winList1.addAll(allWindows1);
		
		driver.switchTo().window(winList1.get(1));
		driver.findElementByName("firstName").sendKeys("b");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(3000);

		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(winList1.get(0));
		
		driver.findElementByLinkText("Merge").click();
		
		driver.switchTo().alert();
		driver.switchTo().alert().accept();
		Thread.sleep(1000);
		
		
		
		
		//driver.close();


	}

}
