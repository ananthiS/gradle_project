package week2.day2;

import java.util.ArrayList;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		List<String> list=new ArrayList<String>();
		list.add("Ananthi");
		list.add("Srinesh");
		list.add("Srinika");
		list.add("Ganesh");
		list.add("Raja");
		
		System.out.println(list.size());
		System.out.println(list.get(1));
		
		for (String eachName : list) {
			if (eachName.startsWith("S")) {
				System.out.println(eachName);
			}
			
		}

		

	}

}
