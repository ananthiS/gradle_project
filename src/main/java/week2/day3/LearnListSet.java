package week2.day3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LearnListSet {

	public static void main(String[] args) {
		learnList();
		learnSet();

	}
	
	public static void learnList() {
		List<String> list= new ArrayList<String>();
		list.add("babu");
		list.add("gopi");
		list.add("gayathri");
		list.add("sarath");
		list.add("raji");
		System.out.println("Size of the list: " +list.size());
		list.add("raji");
		System.out.println("Size of the list after duplicating: " +list.size());
		
		Set<String> newList=new HashSet<String>();
		newList.addAll(list);
		
		System.out.println("size of the list after removing duplicates :" + newList.size());
		
	}

	public static void learnSet() {
		Set<String> set=new HashSet<String>();
		set.add("babu");
		set.add("gopi");
		set.add("gayathri");
		set.add("sarath");
		set.add("raji");
		System.out.println("Size of the set: " +set.size());
	}
}
