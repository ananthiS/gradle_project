package testcases;
import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdmethods.ProjectSpecificMethods;

public class EditLead extends ProjectSpecificMethods {



	//@Test(groups="sanity", dependsOnGroups="smoke")
	@Test(groups="common" ,dataProvider="fetchData")
	public void editLead(String fName,String cName) throws InterruptedException {
		click(locateElement("linktext", "Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(3000);
		//To click the first resulting lead

		click(locateElement("xpath", "(//a[@class='linktext'])[4]"));
		//Thread.sleep(3000);
		verifyTitle("View Lead | opentaps CRM");

		click(locateElement("xpath", "//a[contains(text(),'Edit')]"));
		locateElement("updateLeadForm_companyName").clear();

		type(locateElement("id", "updateLeadForm_companyName"), cName);

		click(locateElement("xpath", "//input[@value='Update']"));

		verifyPartialText(locateElement("viewLead_companyName_sp"), cName);

	}
	@DataProvider(name="fetchData")

	public String[][] fetchData() throws IOException {
		String[][] data=ReadExcel.readCreateLead("EditLead");

		return data;

	}

}
