package testcases;

import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;

public class MergeLead extends ProjectSpecificMethods {
	@Test
	public void mergeLead() throws InterruptedException {
		
		click(locateElement("linktext", "Merge Leads"));
		click(locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]"));
		
		//get window handles and store it in a list
		switchToWindow(1);
		type(locateElement("name", "firstName"), "A");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(3000);
		click(locateElement("xpath", "(//a[@class='linktext'])[1]"));
		
		switchToWindow(0);
		String LeadID=getText(locateElement("xpath", "//input[@name='ComboBox_partyIdFrom']"));
		
		System.out.println(LeadID);
		click(locateElement("linktext", "Merge"));
		acceptAlert();
		click(locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]"));
		
		switchToWindow(1);
		type(locateElement("name", "firstName"), "B");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		
		Thread.sleep(3000);
		click(locateElement("xpath", "(//a[@class='linktext'])[1]"));
		switchToWindow(0);
		click(locateElement("linktext", "Merge"));
		
		acceptAlert();
		Thread.sleep(1000);
	

	}

}
