package testcases;
import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdmethods.ProjectSpecificMethods;

public class DeleteLead extends ProjectSpecificMethods {

	//@Test(groups="reg",dependsOnGroups="smoke")
	@Test(groups="common" ,dataProvider="fetchData")
	public void deleteLead(String phone) throws InterruptedException {

		click(locateElement("linktext", "Find Leads"));


		click(locateElement("xpath", "//span[contains(text(),'Phone')]"));

		type(locateElement("xpath", "//input[@name='phoneNumber']"), phone);	

		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));

		Thread.sleep(3000);

		String leadID = getText(locateElement("xpath", "(//a[@class='linktext'])[4]"));

		System.out.println(leadID);


		click(locateElement("xpath","(//a[@class='linktext'])[4]" ));


		click(locateElement("xpath", "//a[contains(text(),'Delete')]"));
		click(locateElement("linktext", "Find Leads"));



		type(locateElement("xpath", "//input[@name='id']"), leadID);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));


		Thread.sleep(3000);

		String noRecMsg = getText(locateElement("xpath", "//div[contains(text(),'No records to display')]"));
		System.out.println(noRecMsg);

	}
	@DataProvider(name="fetchData")

	public String[][] fetchData() throws IOException {
		String[][] data=ReadExcel.readCreateLead("DeleteLead");

		return data;

	}
}
