package testcases;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utils.ReadExcel;
import wdmethods.ProjectSpecificMethods;

public class CreateLeadNew extends ProjectSpecificMethods {
	@BeforeClass
	public void setData() {
		testcaseName="Create Lead";
		testcaseDesc="Create a new Lead";
		author="Ananthi";
		category="Smoke";
	}
	//@Test(groups="common" ,dataProviderClass=ReadExcel.class, dataProvider="CreateLead")
	@Test(groups="common" ,dataProvider="fetchData")
	public void createLead(String cName,String fName,String lName,String industry,String phone,String email) {
		
		click(locateElement("linktext","Create Lead"));
				
		type(locateElement("id","createLeadForm_companyName"),cName);
				
		type(locateElement("id","createLeadForm_firstName"),fName);
			
		type(locateElement("id","createLeadForm_lastName"),lName);
		selectDropDownUsingText(locateElement("id","createLeadForm_industryEnumId"),industry);
		type(locateElement("id","createLeadForm_primaryPhoneNumber"),phone);
		type(locateElement("id","createLeadForm_primaryEmail"),email);
		click(locateElement("name", "submitButton"));
		}
	
	@DataProvider(name="fetchData")
	
	public String[][] fetchData() throws IOException {
		String[][] data=ReadExcel.readCreateLead("CreateLead");
		
		return data;
		
	}

}
