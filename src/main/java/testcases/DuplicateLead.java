package testcases;


import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;

public class DuplicateLead extends ProjectSpecificMethods {

	//@Test(enabled=false)
	@Test
	public void duplicateLead() throws InterruptedException {
		
		click(locateElement("linktext", "Find Leads"));
		click(locateElement("xpath", "//span[contains(text(),'Email')]"));
		
		type(locateElement("xpath", "//input[@name='emailAddress']"), "anu123@gmail.com");
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(3000);
		click(locateElement("xpath", "(//a[@class='linktext'])[4]"));
		String fName = getText(locateElement("xpath", "//span[@id='viewLead_firstName_sp']"));
		click(locateElement("xpath", "//a[contains(text(),'Duplicate Lead')]"));
		verifyTitle("Duplicate Lead | opentaps CRM");
		click(locateElement("xpath", "//input[@value='Create Lead']"));
		String newfName = getText(locateElement("xpath", "//span[@id='viewLead_firstName_sp']"));
		System.out.println(newfName);
		if (fName.equals(newfName)) {
			System.out.println("Lead duplicated successfully");
		}
		
	}

}
