package testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class Tc001_LoginAndLogout extends SeMethods{

	@Test
	public void login() {
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleUsername = locateElement("id", "username");
	type(eleUsername, "DemoSalesManager");
	WebElement elePassword = locateElement("id", "password");
	type(elePassword, "crmsfa");
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);
	WebElement eleCrmsfalink=locateElement("linktext","CRM/SFA");
	
	click(eleCrmsfalink);
	
	WebElement eleLead=locateElement("linktext","Leads");
	click(eleLead);
	WebElement eleCreateLead=locateElement("linktext","Create Lead");
	click(eleCreateLead);
	
	WebElement eleCompName=locateElement("id","createLeadForm_companyName");
	type(eleCompName,"TestLeaf");
	
	WebElement firstName=locateElement("id","createLeadForm_firstName");
	type(firstName,"Ananthi");
	
	WebElement lastName=locateElement("id","createLeadForm_lastName");
	type(lastName,"Shanmugam");
	
	WebElement sourceEle=locateElement("id","createLeadForm_dataSourceId");
	
	selectDropDownUsingText(sourceEle,"Partner");
	
	WebElement mrktCampId = locateElement("id","createLeadForm_marketingCampaignId");
	selectDropDownUsingText(mrktCampId,"Demo Marketing Campaign");
	
	WebElement firstlocal=locateElement("id","createLeadForm_firstNameLocal");
	type(firstlocal,"anu");
	
	WebElement lastlocal=locateElement("id","createLeadForm_lastNameLocal");
	type(lastlocal,"ganesh");
	
	WebElement title=locateElement("id","createLeadForm_personalTitle");
	type(title,"Salutation");
	
	WebElement proftitle=locateElement("id","createLeadForm_generalProfTitle");
	type(proftitle,"First Lead");
	
	WebElement deptName=locateElement("id","createLeadForm_departmentName");
	type(deptName,"Computer");
	
	WebElement revenue=locateElement("id","createLeadForm_annualRevenue");
	type(revenue,"2423232");
	
	WebElement currency = locateElement("id","createLeadForm_currencyUomId");
	selectDropDownUsingText(currency,"INR - Indian Rupee");
	
	WebElement enumID = locateElement("id","createLeadForm_industryEnumId");
	selectDropDownUsingText(enumID,"Computer Software");
	
	WebElement enumownerID = locateElement("id","createLeadForm_ownershipEnumId");
	selectDropDownUsingText(enumownerID,"Sole Proprietorship");
	
	WebElement numEmpl=locateElement("id","createLeadForm_numberEmployees");
	type(numEmpl,"24");
	
	WebElement siccode=locateElement("id","createLeadForm_sicCode");
	type(siccode,"SIC Code");
	
	WebElement tickersymbl=locateElement("id","createLeadForm_tickerSymbol");
	type(tickersymbl,"Ticker Symbol");
	
	WebElement desc=locateElement("id","createLeadForm_description");
	type(desc,"sdsdsdsd");
	WebElement impnote=locateElement("id","createLeadForm_importantNote");
	type(impnote,"sdsdsadsdsddsdwqqwewqeew");
	WebElement countrycode=locateElement("id","createLeadForm_primaryPhoneCountryCode");
	type(countrycode,"521");
	WebElement areacode=locateElement("id","createLeadForm_primaryPhoneAreaCode");
	type(areacode,"001");
	WebElement phone=locateElement("id","createLeadForm_primaryPhoneNumber");
	type(phone,"9566758373");
	WebElement extn=locateElement("id","createLeadForm_primaryPhoneExtension");
	type(extn,"5858");
	
	WebElement name=locateElement("id","createLeadForm_primaryPhoneAskForName");
	type(name,"bala");
	WebElement mail=locateElement("id","createLeadForm_primaryEmail");
	type(mail,"anu123@gmail.com");
	WebElement toname=locateElement("id","createLeadForm_generalToName");
	type(toname,"sdsadqkle3e");
	WebElement attnname=locateElement("id","createLeadForm_generalAttnName");
	type(attnname,"sdsadsads");
	WebElement addr1=locateElement("id","createLeadForm_generalAddress1");
	type(addr1,"Address1");
	WebElement addr2=locateElement("id","createLeadForm_generalAddress2");
	type(addr2,"Address2");
	WebElement city=locateElement("id","createLeadForm_generalCity");
	type(city,"City");
	
	WebElement state = locateElement("id","createLeadForm_generalStateProvinceGeoId");
	selectDropDownUsingText(state,"Washington");

	
	WebElement geoid=locateElement("id","createLeadForm_generalStateProvinceGeoId");
	type(geoid,"5858");
	
	
	WebElement postalCode=locateElement("id","createLeadForm_generalPostalCode");
	type(postalCode,"600073");
	WebElement phoneextn=locateElement("id","createLeadForm_primaryPhoneExtension");
	type(phoneextn,"5858");
	WebElement postalextn=locateElement("id","createLeadForm_generalPostalCodeExt");
	type(postalextn,"5858");
	
	WebElement clkSubmit=locateElement("name", "submitButton");
	click(clkSubmit);

	
	}
	
}







