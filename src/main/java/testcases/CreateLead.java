package testcases;

import org.testng.annotations.Test;

import wdmethods.ProjectSpecificMethods;

public class CreateLead extends ProjectSpecificMethods {
	
	@Test(groups="smoke")
	public void createLead() {
		
		click(locateElement("linktext","Create Lead"));
		
		
		type(locateElement("id","createLeadForm_companyName"),"TestLeaf");
		
		
		type(locateElement("id","createLeadForm_firstName"),"Ananthi");
		
		
		type(locateElement("id","createLeadForm_lastName"),"Shanmugam");
		selectDropDownUsingText(locateElement("id","createLeadForm_dataSourceId"),"Partner");
		
		
		selectDropDownUsingText(locateElement("id","createLeadForm_marketingCampaignId"),"Demo Marketing Campaign");
		
		
		type(locateElement("id","createLeadForm_firstNameLocal"),"anu");
		
		
		type(locateElement("id","createLeadForm_lastNameLocal"),"ganesh");
		
		
		type(locateElement("id","createLeadForm_personalTitle"),"Salutation");
		
		
		type(locateElement("id","createLeadForm_generalProfTitle"),"First Lead");
		
		
		type(locateElement("id","createLeadForm_departmentName"),"Computer");
		
		
		type(locateElement("id","createLeadForm_annualRevenue"),"2423232");
		
		
		selectDropDownUsingText(locateElement("id","createLeadForm_currencyUomId"),"INR - Indian Rupee");
		
		
		selectDropDownUsingText(locateElement("id","createLeadForm_industryEnumId"),"Computer Software");
		
		
		selectDropDownUsingText(locateElement("id","createLeadForm_ownershipEnumId"),"Sole Proprietorship");
		
		
		type(locateElement("id","createLeadForm_numberEmployees"),"24");
		
		
		type(locateElement("id","createLeadForm_sicCode"),"SIC Code");
		
		
		type(locateElement("id","createLeadForm_tickerSymbol"),"Ticker Symbol");
		
		
		type(locateElement("id","createLeadForm_description"),"sdsdsdsd");
		
		type(locateElement("id","createLeadForm_importantNote"),"sdsdsadsdsddsdwqqwewqeew");
		
		type(locateElement("id","createLeadForm_primaryPhoneCountryCode"),"521");
		
		type(locateElement("id","createLeadForm_primaryPhoneAreaCode"),"001");
		
		type(locateElement("id","createLeadForm_primaryPhoneNumber"),"9566758373");
		
		type(locateElement("id","createLeadForm_primaryPhoneExtension"),"5858");
		
		
		type(locateElement("id","createLeadForm_primaryPhoneAskForName"),"bala");
		
		type(locateElement("id","createLeadForm_primaryEmail"),"anu123@gmail.com");
		
		type(locateElement("id","createLeadForm_generalToName"),"sdsadqkle3e");
		
		type(locateElement("id","createLeadForm_generalAttnName"),"sdsadsads");
		
		type(locateElement("id","createLeadForm_generalAddress1"),"Address1");
		
		type(locateElement("id","createLeadForm_generalAddress2"),"Address2");
		
		type(locateElement("id","createLeadForm_generalCity"),"City");
		
		
		selectDropDownUsingText(locateElement("id","createLeadForm_generalStateProvinceGeoId"),"Washington");

		
		
		type(locateElement("id","createLeadForm_generalStateProvinceGeoId"),"5858");
		
		
		
		type(locateElement("id","createLeadForm_generalPostalCode"),"600073");
		
		type(locateElement("id","createLeadForm_primaryPhoneExtension"),"5858");
		
		type(locateElement("id","createLeadForm_generalPostalCodeExt"),"5858");
		
		
		click(locateElement("name", "submitButton"));
		}

}
