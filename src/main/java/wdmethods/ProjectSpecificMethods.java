package wdmethods;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

public class ProjectSpecificMethods extends SeMethods {
	
	public String testcaseName,testcaseDesc,author,category;
	
	@Parameters({"url","username","pwd","browser"})
	@BeforeMethod(groups="common")
	public void login(String url,String username,String pwd,String browser) {
		
		assignTest(testcaseName, testcaseDesc, author, category);
		startApp(browser, url);
		type(locateElement("id","username"), username);
		type(locateElement("id", "password"), pwd);
		click(locateElement("class", "decorativeSubmit"));
		click(locateElement("linktext", "CRM/SFA"));
		click(locateElement("linktext", "Leads"));
		
	}
	@AfterMethod(groups="common")
	public void close() {
		closeBrowser();
		
	}
	@BeforeSuite(groups="common")
	public void beforeSuite() {
		startResult();
	}
	@AfterSuite(groups="common")
	public void afterSuite() {
		endResult();
	}
}
