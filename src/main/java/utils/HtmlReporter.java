package utils;



import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import wdmethods.ProjectSpecificMethods;

public abstract class HtmlReporter {
	static ExtentHtmlReporter html;
	static ExtentReports extent;
	ExtentTest test;

	//before suite	
	public void startResult() {
		//creates a new html page
		html= new ExtentHtmlReporter("./reports/extentReports.html"); 
		html.setAppendExisting(true);
		extent=new ExtentReports();
		extent.attachReporter(html);
	}
		
		//before the method for every testcase		
		public void assignTest(String testcaseName,String testcaseDesc,String author,String category) {
			test=extent.createTest(testcaseName,testcaseDesc);
			test.assignAuthor(author);
			test.assignCategory(category);
		}
		public abstract long takeSnap();
			
		//these steps in try and catch
		public void logSteps(String status,String log) {
			long takeSnap=100000L;
			takeSnap=takeSnap();
			//MediaEntityBuilder build=null;
			MediaEntityModelProvider build=null;
			try {
				build = MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img"+takeSnap+".png").build();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (status.equalsIgnoreCase("pass")) {
				test.pass(log);
			}else if(status.equalsIgnoreCase("fail")){
				test.fail(log);
			}else if(status.equalsIgnoreCase("warning")) {
				test.warning(log);
			}
			
					
		/*test.pass("user name entered",
				MediaEntityBuilder
				.createScreenCaptureFromPath
				(".././snaps/dashboard.png").build()); */
		}
		//After suite
		public void endResult() {
			extent.flush();
		}
				
}		
		

	


