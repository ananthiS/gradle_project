package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAdvXpath {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		
		//enter login name
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByLinkText("Find Leads").click();
		
		driver.findElementByXPath("//span[contains(text(),'Phone')]").click();
		driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys("9566758373");
		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
		
		Thread.sleep(3000);
		String phoneNum = driver.findElementByXPath("//div[contains(text(),'9566758373')]").getText();
		System.out.println(phoneNum);

	}

}
