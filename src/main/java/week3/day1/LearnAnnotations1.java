package week3.day1;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class LearnAnnotations1 {
	@Test
	public void anotherLogin() {		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		//To load URL
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		//enter login name
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		//To click Leads tab
		driver.findElementByLinkText("Leads").click();
		//To click Create Lead
		driver.findElementByLinkText("Create Lead").click();
}

}
